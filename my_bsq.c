/*
** my_bsq.c for  in /home/karmes_l/Projets/Prog_Elem/BSQ
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec 23 10:30:19 2014 lionel karmes
** Last update Thu Jan  8 16:19:26 2015 lionel karmes
*/

#include "my.h"

void		print_map(char **map, t_cursor *cursor, int height_carre)
{
  int		y;
  int		x;

  y = 0;
  while (map[y] != 0)
    {
      x = 0;
      while (x < my_strlen(map[0]))
	{
	  if (x >= cursor->x_carre && x < cursor->x_carre + height_carre
	      && y >= cursor->y_carre && y < cursor->y_carre + height_carre)
	    my_putchar('x');
	  else
	    my_putchar(map[y][x]);
	  x++;
	}
      if (map[y + 1] != 0)
	my_putchar('\n');
      y++;
    }
}

int		height(char **map)
{
  int		y;

  y = 0;
  while (map[y] != 0)
    y++;
  return (y);
}

int		my_bsq(char **av)
{
  t_cursor	cursor;
  char		**map;
  int		height_map;

  if ((map = init_map(av[1])) == NULL)
    return (0);
  height_map = height(map);
  print_map(map, &cursor, find_carre(map, height_map, &cursor));
  return (1);
}
