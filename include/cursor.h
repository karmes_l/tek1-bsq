/*
** cursor.h for  in /home/karmes_l/Projets/Prog_Elem/BSQ
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Jan  8 11:01:46 2015 lionel karmes
** Last update Thu Jan  8 16:16:52 2015 lionel karmes
*/
#ifndef CURSOR_H_
# define CURSOR_H_

typedef struct	s_cursor
{
  int		x;
  int		y;
  int		x_carre;
  int		y_carre;
}		t_cursor;

#endif /* !CURSOR_H_ */
