/*
** my.h for  in /home/karmes_l/test/tmp_Piscine_C_J09
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 15:22:30 2014 lionel karmes
** Last update Sun Jan 18 15:37:09 2015 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include "get_next_line.h"
# include "cursor.h"

char		*convert_base(char *, char *, char *);
int		count_num(long nb);
int		my_charisnum(char);
void		my_putchar(char);
void		my_putnbr(long);
void		my_swap(int *, int *);
void		my_putstr(char *);
int		my_strlen(char *);
int		my_getnbr(char *);
void		my_sort_int_tab(int *, int);
char		*mu_strcpy(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_revstr(char *);
char		*my_strstr(char *, char *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int n);
char		*my_strupcase(char *);
char		*my_strlowcase(char *);
char		*mystrcapitalize(char *);
int		my_str_isalpha(char *);
int		my_str_isnum(char *);
int		mu_str_islower(char *);
int		my_str_isupper(char *);
int		my_str_isprintable(char *);
char		*my_strcat(char *, char *);
char		*my_strcpy(char *, char *);
char		*my_strncat(char *, char *, int);
int		my_strlcat(char *, char *, int);
int		my_str_isnum2(char *);
unsigned long	pow_10(int);
int		power(unsigned long, unsigned long);
char		**my_str_to_wordtab(char *);
int		my_bsq(char **);
char		**init_map(char *);
int		find_carre(char **, int, t_cursor *);

# define SIZE_WIN_X (1000)
# define SIZE_WIN_Y (1000)
# define SIZE_MAP (20)

#endif /* !MY_H_ */
