##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Thu Jan  8 11:01:26 2015 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/

LDFLAGS	=

NAME	= bsq

LIB	= libmy

SRCS	= main.c \
	my_bsq.c \
	get_next_line.c \
	open_map.c \
	find_carre.c

OBJS	= $(SRCS:.c=.o)


all: $(NAME)

$(NAME): $(OBJS)
	make -C lib/
	$(CC) $(OBJS) -o $(NAME) $(LDFLAGS) -L./lib/ -lmy -I./include/

clean:
	$(RM) $(OBJS)
	make clean -C lib/

fclean: clean
	make fclean -C lib/
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
