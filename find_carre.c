/*
** find_carree.c for  in /home/karmes_l/Projets/Prog_Elem/BSQ
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Jan  8 11:00:05 2015 lionel karmes
** Last update Sun Jan 18 15:28:56 2015 lionel karmes
*/

#include "my.h"

int		carre(char **map, int height_map, int x, int y)
{
  int		i;
  int		k;

  i = 0;
  while (y + i < height_map && x + i < my_strlen(map[0]) &&
	 map[y + i][x + i] == '.')
    {
      k = 0;
      while (k <= i)
      	{
      	  if (map[y + i][x + k] != '.' || map[y + k][x + i] != '.')
	    return (i);
      	  k++;
      	}
      i++;
    }
  return (i);
}

int		find_carre(char **map, int height_map, t_cursor *cursor)
{
  int		height_carre;
  int		tmp;

  cursor->x = 0;
  cursor->y = 0;
  height_carre = 0;
  while (cursor->y < height_map)
    {
      while (cursor->x < my_strlen(map[0]))
      	{
      	  tmp = carre(map, height_map, cursor->x, cursor->y);
      	  if (tmp > height_carre)
      	    {
      	      height_carre = tmp;
      	      cursor->x_carre = cursor->x;
      	      cursor->y_carre = cursor->y;
      	    }
      	  cursor->x++;
      	}
      cursor->x = 0;
      cursor->y++;
    }
  return (height_carre);
}
