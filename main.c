/*
** main.c for  in /home/karmes_l/Projets/Prog_Elem/BSQ
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec 23 10:29:34 2014 lionel karmes
** Last update Thu Jan  8 10:54:26 2015 lionel karmes
*/

#include "my.h"

int	main(int ac, char **av)
{
  if (ac == 2)
    my_bsq(av);
  else
    my_putstr("Usage : ./bsq [FILE]");
  my_putchar('\n');
  return (0);
}
