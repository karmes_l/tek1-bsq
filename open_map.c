/*
** open_map.c for  in /home/karmes_l/Projets/Prog_Elem/BSQ
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec 23 10:40:28 2014 lionel karmes
** Last update Mon Jan 12 13:51:34 2015 lionel karmes
*/

#include "my.h"

char	**sort_map(int fd, char **map, int len)
{
  int	y;
  char	*s;
  int	str_len;

  y = 0;
  while (y < len && (s = get_next_line(fd)) != NULL)
    {
      if ((map[y] = malloc(sizeof(char) * (my_strlen(s) + 1))) == NULL)
	exit(1);
      if ((y > 0 && str_len != my_strlen(s)) || my_strlen(s) == 0)
	{
	  my_putstr("[ERROR] : largeur du rectangle invalide");
	  free(s);
	  return (NULL);
	}
      str_len = my_strlen(s);
      my_strcpy(map[y], s);
      free(s);
      y++;
    }
  return (map);
}

char	**init_map(char *path)
{
  char	**map;
  int	fd;
  int	len;
  char	*s;

  if ((fd = open(path, O_RDONLY)) == -1)
    {
      my_putstr("[ERROR] : open file\n");
      exit(1);
    }
  s = get_next_line(fd);
  if (s != NULL)
    len = my_getnbr(s);
  free(s);
  if ((map = malloc(sizeof(char *) * (len + 1))) == NULL)
    exit(1);
  map[len] = 0;
  map = sort_map(fd, map, len);
  close(fd);
  return (map);
}
